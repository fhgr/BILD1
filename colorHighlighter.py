# -*- coding: utf-8 -*-
"""
colorHighlighter.py

color highlighter app / color segmentation demo
Displays the video stream of the Webcam.
Highlights red/yellow/green/blue.

based on mplVideoFig

Created on Mon Sep 21 09:09:19 2020

@author: birkudo

"""

__author__ = "Udo Birk"
__copyright__ = "Copyright 2020, University of Applied Siences of the Grisons, Switzerland"
__credits__ = ["Udo Birk"]
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Udo Birk"
__email__ = "udo.birk@fhgr.ch"
__status__ = "Production"


import sys
import cv2
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QImage, qRgb

class NotImplementedException:
    pass

gray_color_table = [qRgb(i, i, i) for i in range(256)]

def toQImage(im, copy=True):
    """
    helper function toQImage(im).
    copying the image or the figure content to the clipboard  
    uses PyQt5. toQImage implements the conversion between numpy and QImage.
    """
    if im is None:
        return QImage()

    if im.dtype == np.uint8:
        if len(im.shape) == 2:
            qim = QImage(im.data, im.shape[1], im.shape[0], im.strides[0], QImage.Format_Indexed8)
            qim.setColorTable(gray_color_table)
            return qim.copy() if copy else qim

        elif len(im.shape) == 3:
            if im.shape[2] == 3:
                qim = QImage(im.data, im.shape[1], im.shape[0], im.strides[0], QImage.Format_RGB888);
                return qim.copy() if copy else qim
            elif im.shape[2] == 4:
                qim = QImage(im.data, im.shape[1], im.shape[0], im.strides[0], QImage.Format_ARGB32);
                return qim.copy() if copy else qim

    raise NotImplementedException
    
class colorHighlighterApp(object):
	"""
	Main object class.
	Opens the video stream to the webcam using openCV. 
	Displays the current frame, highlighting red/yellow/green/blue.
	The main calculations happen in the function: update().
	A timer is used to repeatedly call update().
	"""
	fig = None;
	ax  = None;
	cap = None;
	img_handle = None;
	isRunning = False;
	aspect = 0;  # default: 'equal', showing box
	bFlipImage = False;
    
	def open_cap_settings_dlg(self, event=None):
		self.cap.set(cv2.CAP_PROP_SETTINGS,0);
    
	def copy_fig_to_clipboard(self):
		pixmap = self.fig.canvas.grab()
		QApplication.clipboard().setImage(pixmap.toImage())
		print('figure %d copied to clipboard' % self.fig.number)

	def copy_RGBimg_to_clipboard(self):
		img = self.img_handle.get_array();
		# h,w = img.shape[:2];
		# qimage = QImage(img.data, h, w, 3*h, QImage.Format_RGB888)
		qimage = toQImage(img, copy=True)
		QApplication.clipboard().setImage(qimage)
		print('figure %d copied to clipboard' % self.fig.number)
	
	def __init__(self, deviceID = None):
		self.fig, self.ax = plt.subplots();
		self.fig.canvas.set_window_title('colorHighlighter | Ctrl+c = copy image, SPACE = pause')
		# self.fig = plt.figure()
		if(deviceID == None):
			deviceID = 0;
			
		self.cap = cv2.VideoCapture(deviceID)
		if not self.cap.isOpened():
			print("ERROR: Could not open VideoDevice")
			plt.close(self.fig);
			del(self);
			return None;
		
		ret, frame = self.cap.read();
		if(not ret):
			print("ERROR: Could not read from VideoDevice")
			plt.close(self.fig);
			del(self);
			return None;
			
		frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
		self.img_handle = self.ax.imshow(frame)
			
		self.timer = self.fig.canvas.new_timer(interval=200)
		# self.timer.add_callback(CloseEvent())
		self.timer.add_callback(self.update)
		self.start()
		plt.show()
		self.fig.canvas.mpl_connect('close_event', self.handle_close)
		self.fig.canvas.mpl_connect('key_press_event', self.on_key_event)
        
#		axSettings = plt.axes([0.0, 0.95, 0.15, 0.05])
#		buttonSettings = matplotlib.widgets.Button(axSettings, 'Settings ...')
#		buttonSettings.on_clicked(self.open_cap_settings_dlg)

	def start(self):        
		self.isRunning = True;
		self.timer.start();

	def stop(self):        
		self.timer.stop();
		self.isRunning = False;

	def on_key_event(self, event):
		# print('press', event.key)
		# sys.stdout.flush()

		# if event.key == 'ctrl+c' or event.key == 'ctrl+C':
		if event.key == 'ctrl+c':
			self.copy_RGBimg_to_clipboard();
		if event.key == 'ctrl+C':
			self.copy_fig_to_clipboard();
		if event.key == ' ':   # toggle pause
			if(self.isRunning):
				self.stop();
			else:
				self.start();
		if event.key == 'f5':  # toggle aspect ratio setting
			self.toggleAspect();
		if event.key == 'f6':  # toggle flip image left/right
			self.bFlipImage = not self.bFlipImage;

	def toggleAspect(self):        
		self.aspect = (self.aspect + 1) % 4;
		if(self.aspect == 0):
			self.ax.set_position(matplotlib.transforms.Bbox(np.array([[0.152777,0.109999],[0.872222,0.88]])))
			self.ax.set_aspect('equal')
		if(self.aspect == 1):
			self.ax.set_position(matplotlib.transforms.Bbox(np.array([[0.152777,0.109999],[0.872222,0.88]])))
			self.ax.set_aspect('auto')
		if(self.aspect == 2):
			self.ax.set_position((0,0,1,1))
			self.ax.set_aspect('equal')
		if(self.aspect == 3):
			self.ax.set_position((0,0,1,1))
			self.ax.set_aspect('auto')
		self.fig.canvas.draw()
		print("aspect setting: ", self.aspect)
		sys.stdout.flush()

	def handle_close(self, evt):
		print("deleting ocvVideoFig object ...")
		self.timer.stop();
		try:
			self.cap.release();
		except:
			pass;

	def close_event(self):
		self.timer.stop();
		try:
			self.cap.release();
		except:
			pass;
		try:
			plt.close(self.fig)
		except:
			pass;

	def update(self):
		# print("updating ...")
		ret, frame = self.cap.read()
		
		if(not ret):
			print("Could not read from camera");
			self.close_event();
			
		b,g,r = cv2.split(frame);
		HSV = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV);
		h,s,v = cv2.split(HSV);
		# print("max h:{}, max s:{}, max v:{}".format(np.max(h),np.max(s),np.max(v)))
		# mask = np.bitwise_or(h < 20, h > 320);
		# r[mask] = 255; g[mask]=0; b[mask]= 92;
		# mask  = np.bitwise_and(float(s)*float(v)/65025 > 0.5);
		maskL  = (s.astype(np.float64)*v.astype(np.float64)/65025 > 0.4);
		maskG  = (s.astype(np.float64)*v.astype(np.float64)/65025 > 0.16);
		maskD  = (s.astype(np.float64)*v.astype(np.float64)/65025 > 0.28);
		maskR = maskL * np.bitwise_or(h < 8, h > 125);
		maskY = maskL * np.bitwise_and(h > 17, h <  43);
		maskG = maskG * np.bitwise_and(h > 42, h < 85);
		maskB = maskD * np.bitwise_and(h > 97, h < 135);
		rn = (0.7*r).astype(np.uint8); gn = (0.7*g).astype(np.uint8); bn = (0.7*b).astype(np.uint8);
		rn[maskR] = 250; gn[maskR] =   0; bn[maskR] =  92;
		rn[maskY] = 250; gn[maskY] = 255; bn[maskY] =   0;
		rn[maskG] = 104; gn[maskG] = 255; bn[maskG] =   0;
		rn[maskB] =   0; gn[maskB] = 181; bn[maskB] = 255;
		# frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
		frame = cv2.merge((rn,gn,bn));
		# frame = 255.*maskB.astype(np.float).copy();
		if(self.bFlipImage):
			frame = cv2.flip(frame,1)
		self.img_handle.set_array(frame)
		self.fig.canvas.draw_idle();
		
		
		
class CloseEvent(object):

    def __init__(self):
        self.first = True

    def __call__(self):
        if self.first:
            self.first = False
            return
        sys.exit(0)

if __name__=="__main__":
	cfig = colorHighlighterApp(0)
	setattr(cfig.fig,'ocvVideoFig',cfig);
	cfig.open_cap_settings_dlg()
    