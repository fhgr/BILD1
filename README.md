# BILD1

Tools and snippets for the lecture series Bildverarbeitung1.

contains:


_________________

**[HSVcolorRangeThresholder.py](./HSVcolorRangeThresholder.py)**  
Display a GUI to select a color-range in HSV color space.  
(See also [https://medium.com/programming-fever/how-to-find-hsv-range-of-an-object-for-computer-vision-applications-254a8eb039fc])  
Press 'p' to print the HSV selection values plus python code to the command line.  
![HSVcolorRangeThresholder screenshot](./HSVcolorRangeThresholder_Screenshot.png "HSVcolorRangeThresholder_Screenshot")

_________________

**[colorSelection.py](./colorSelection.py)**  
Display a GUI to select a color in HSV color space. (See also [https://www.w3schools.com/colors/colors_picker.asp])
![colorSelection screenshot](./colorSelection_Screenshot.png "colorSelection_Screenshot")

_________________

**[colorHighligher.py](./colorHighligher.py)**  
Opens a live webcam viewer, and displays the video stream highlighting red/yellow/green/blue colors.
![colorHighlighter screenshot](./colorHighlighter_Screenshot.png "colorHighlighter_Screenshot")
