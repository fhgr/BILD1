# -*- coding: utf-8 -*-
"""
colorSelection.py

Displays the colorSelection dialog.
The user can interactively choose a color in HSV color space.

Created on Fri Sep 11 08:43:10 2020

@author: birkudo

see also: https://www.w3schools.com/colors/colors_picker.asp
"""

__author__ = "Udo Birk"
__copyright__ = "Copyright 2020, University of Applied Siences of the Grisons, Switzerland"
__credits__ = ["Udo Birk"]
__license__ = "GPL"
__version__ = "1.0.2"
__maintainer__ = "Udo Birk"
__email__ = "udo.birk@fhgr.ch"
__status__ = "Production"

import cv2
import numpy as np

import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.widgets import Slider
import matplotlib as mpl

INTNORM = 255.9999

def debout(str):
    """
    Depending on the setting of the local variable debug,
    text is output to the command line (debug=True) or not.
    >>> x=1.5;debout('The value of x is {}'.format(x))
    1.0
    """
    debug = False;
    if debug:
        print(str)

class axSlider(object):
    """
    Variant of the matplotlib widget slider.
    Can be set up with a single line of comand.
    Keeps track of the attached variable.
    Issues a notification, if actuated, and calls attached callback function.
    Can be updated without calling the attached callback function.
    """
    axcolor = 'lightgoldenrodyellow'
    def __init__(self, fig, name, position, callback=None, minVal=0, maxVal=255, valinit=None, valstep=1, ):
        debout('slider position={}'.format(position))
        self.name = name
        self.callback = callback
        self.ax = fig.add_axes(position, facecolor=self.axcolor)
        if(valinit is None):
            valinit = maxVal
        self.val = valinit;
        self.slider = Slider(self.ax, self.name, minVal, maxVal, valinit=valinit, valstep=valstep, valfmt="%i")
        self.slider.on_changed(self.update)
        
    def update(self,val):
        self.val = val
        self.callback(self, val)
        
    def set_val(self, val, notify=True):
        self.val = val;
        if not notify:
            self.slider.eventson=False
            self.slider.set_val(val)
            self.slider.eventson=True
        else:
            self.slider.set_val(val)
            
    def increase(self, notify=True):
        val = np.round(self.val + self.slider.valstep);
        if (val > self.slider.valmax):
            self.set_val(self.slider.valmin);
        else:
            self.set_val((val));

    def decrease(self, notify=True):
        val = np.round(self.val - self.slider.valstep);
        if (val < self.slider.valmin):
            self.set_val(self.slider.valmax);
        else:
            self.set_val((val));

class colorSelection():
    """
    Main object class.
    Displays the color selection dialog.
    """
    # GUI elements:
    fig = None
    wheelAxis = None
    sliders = {}
    buttonDownAxis = None
    hsvAxis = None
    hsvImg = None
    previewAxis = None
    previewImg = None
    lastSlider = None;
    # color variables using the HSV color space
    hue = 0.0
    sat = 1.0
    val = 1.0
    hueHack = 0.0
    
    def onclick(self, event):
        if event.inaxes == self.wheelAxis:
            debout('wheelAxis %s click: button=%d, x=%d, y=%d, xdata=%f, ydata=%f' %
                  ('double' if event.dblclick else 'single', event.button,
                   event.x, event.y, event.xdata, event.ydata))
            x = event.xdata;
            y = -event.ydata;
            self.hue = 180*(np.arctan2(x,y)/np.pi+1)
            debout('WHEEL: x,y,hue={:3.2f},{:3.2f}: {:5.1f}'.format(x,y,self.hue))
            self.buttonDownAxis = self.wheelAxis;
            self.updateImages(True);
        elif event.inaxes == self.hsvAxis:
            debout('hsvAxis %s click: button=%d, x=%d, y=%d, xdata=%f, ydata=%f' %
                  ('double' if event.dblclick else 'single', event.button,
                   event.x, event.y, event.xdata, event.ydata))
            x = event.xdata;
            y = event.ydata;
            x = min(max(x,0.),1.);
            y = 1.-min(max(y,0.),1.);
            self.sat = x
            self.val = y
            debout('HSV: x,y,hue={:3.2f},{:3.2f}: {:5.1f}'.format(x,y,self.hue))
            self.sliders['sat'].set_val(np.min((255.,INTNORM*self.sat)),False)
            self.sliders['val'].set_val(np.min((255.,INTNORM*self.val)),False)
            self.buttonDownAxis = self.hsvAxis;
            self.updateImages(True);
        else:
            debout(event) 
    
    def onMouseMove(self, event):
        if event.inaxes == self.wheelAxis:
            if self.buttonDownAxis == self.wheelAxis:
                x = event.xdata;
                y = -event.ydata;
                self.hue = 180*(np.arctan2(x,y)/np.pi+1)
                debout('move {:3.2f},{:3.2f}: {:5.1f}'.format(x,y,self.hue))
                self.sliders['hue'].set_val(self.hue,False)
                self.updateImages(True);
        if event.inaxes == self.hsvAxis:
            if self.buttonDownAxis == self.hsvAxis:
                x = event.xdata;
                y = event.ydata;
                x = min(max(x,0.),1.);
                y = 1.-min(max(y,0.),1.);
                self.sat = x
                self.val = y
                debout('move {:3.2f},{:3.2f}: {:5.1f}'.format(x,y,self.hue))
                self.sliders['sat'].set_val(np.min((255.,INTNORM*self.sat)),False)
                self.sliders['val'].set_val(np.min((255.,INTNORM*self.val)),False)
                self.updateImages(True);
        else:
            pass

    def onrelease(self, event)        :
        self.buttonDownAxis = None;
    
    def updateSlider(self, axSlider, val):
        updateRGB = False
        rgb=False;
        debout("Updating slider with name='{}'".format(axSlider.name))
        self.lastSlider = axSlider;
        if(axSlider.name == 'hue'):
            self.hue = val;
            updateRGB = True;
        if(axSlider.name == 'sat'):
            self.sat = val/INTNORM;
            updateRGB = True;
        if(axSlider.name == 'val'):
            self.val = val/INTNORM;
            updateRGB = True;
        if(axSlider.name == 'R'):
            rgb=True;
        if(axSlider.name == 'G'):
            rgb=True;
        if(axSlider.name == 'B'):
            rgb=True;
        if(rgb):
            r = self.sliders['R'].val/INTNORM;
            g = self.sliders['G'].val/INTNORM;
            b = self.sliders['B'].val/INTNORM;
            dst = cv2.cvtColor(np.array([[[r,g,b]]]).astype(np.float32),cv2.COLOR_RGB2HSV);
            h,s,v = cv2.split(dst);
            debout('RGB={},{},{}   \nHSV={},{},{}'.format(r,g,b,h,s,v))
            if np.sum(1-np.hstack((r,g,b)))==0:
                self.hueHack = self.hue
            elif np.sum(np.hstack((r,g,b)))==0:
                self.hueHack = self.hue
            else:
                self.hue = h[0][0];
            self.sat = s[0][0];
            self.val = v[0][0];
        self.updateImages(updateRGB);
        
    def updateImages(self, updateRGB = False):
        debout(self.hue)
        x = np.linspace(0,1,512)
        s,v = np.meshgrid(x,x)
        # dst = self.hsvImg.get_data();
        h = np.zeros((512,512),np.float32)+self.hue
        dst = cv2.cvtColor(np.dstack((h,s,v)).astype(np.float32),cv2.COLOR_HSV2RGB);
        # hsvImg.set_data()
        self.hsvImg.set_data(dst)
        dst = cv2.cvtColor(np.array([[[self.hue,self.sat,self.val]]]).astype(np.float32),cv2.COLOR_HSV2RGB);
        debout('updating images. \nRGB color={}'.format(dst))
        # hsvImg.set_data()
        self.previewImg.set_data(dst)
        self.fig.canvas.draw()
        inverseRGB=dst[0][0];
        inverseRGB=np.array(1.-np.array(inverseRGB));
        # inverseRGB = cv2.cvtColor(np.array([[[(self.hue+180.) % 360,1.0,0.5]]]).astype(np.float32),cv2.COLOR_HSV2RGB)[0][0]
        self.hsvCursor.set_markeredgecolor(inverseRGB)
        self.hsvCursor.set_data(self.sat,1.-self.val)
        r,g,b = cv2.split(dst.astype(np.float64));
        r = np.float(r);
        rd = np.min((1.,r*INTNORM/255.)); 
        g = np.float(g);
        gd = np.min((1.,g*INTNORM/255.)); 
        b = np.float(b);
        bd = np.min((1.,b*INTNORM/255.)); 
        print("RGB={},{},{}".format(rd,gd,bd))
        html = mpl.colors.to_hex(np.hstack(([[rd]],[[gd]],[[bd]]))[0]);
        I = (rd+gd+bd)/3.;
        try:
            S = 1.-1./I*np.min((rd,gd,bd))
        except:
            S = 0;
        html+="\nHue:{:4.1f}\nSat:{:3.1f}%\nI:{:3.1f}%".format(self.hue, S*100., I*100.)
        self.htxt.set_text(html)
        try:
            self.sliders['R'].set_val(np.min((255.,INTNORM*r)),False);
            self.sliders['G'].set_val(np.min((255.,INTNORM*g)),False);
            self.sliders['B'].set_val(np.min((255.,INTNORM*b)),False);
            self.sliders['hue'].set_val(self.hue,False);
            self.sliders['sat'].set_val(np.min((255.,INTNORM*self.sat)),False);
            self.sliders['val'].set_val(np.min((255.,INTNORM*self.val)),False);
        except:
            pass;
            
        angle = self.hue*np.pi/180.
        x = -np.sin(angle)*np.array([0.8,0.9]);
        y = np.cos(angle)*np.array([0.8,0.9]);
        self.hueIndicator.set_data(x,y)
        # plt.show()

    def on_keypress(self,event):        
        if event.key == 'left':
            self.lastSlider.decrease();
        if event.key == 'right':
            self.lastSlider.increase();
        
    def __init__(self):
        self.fig = plt.figure(figsize=(7.5,3.38))
        self.fig.canvas.set_window_title('colorSelection.py')
        if(1==2):
            self.wheelAxis = self.fig.add_axes([0.01,0.05,0.4,0.9], projection='polar')
            self.wheelAxis._direction = 2*np.pi ## This is a nasty hack - using the hidden field to 
                                              ## multiply the values such that 1 become 2*pi
                                              ## this field is supposed to take values 1 or -1 only!!
            
            norm = mpl.colors.Normalize(0.0, 2*np.pi)
            
            # Plot the colorbar onto the polar axis
            # note - use orientation horizontal so that the gradient goes around
            # the wheel rather than centre out
            quant_steps = 2056
            cb = mpl.colorbar.ColorbarBase(self.wheelAxis, cmap=cm.get_cmap('hsv',quant_steps),
                                               norm=norm,
                                               orientation='horizontal')
            
            # aesthetics - get rid of border and axis labels                                   
            cb.outline.set_visible(False)                                 
            self.wheelAxis.set_rlim([-1,0.5])
        else:
            self.wheelAxis = self.fig.add_axes([0.0,0.0,0.45,1.0])
            x = np.linspace(-1,1,511)
            xv, yv = np.meshgrid(x, x)
            r = np.sqrt(xv**2+yv**2);
            mask = np.bitwise_and(r <= 0.9,r >= 0.8);
            arg = (np.arctan2(xv,yv)/np.pi*179.99999).astype(np.float32)+180
            dst = np.zeros((511,511,3),np.float32)
            h, s, v    = cv2.split(dst)
            v += 1.0;
            h[mask]=arg[mask]
            s[mask]=1.0;
            dst = cv2.cvtColor(np.dstack((h,s,v)).astype(np.float32),cv2.COLOR_HSV2RGB);
            self.wheelAxis.imshow(dst, extent=(-1,1,-1,1))
            
        self.wheelAxis.set_axis_off()
        
        self.hsvAxis = self.fig.add_axes([0.46,0.05,0.2,0.5])
        self.hsvImg = self.hsvAxis.imshow(np.zeros((512,512,3),np.float32),extent=[0,1,0,1])
        self.hsvAxis.set_xlim(-0.1,1.1)
        self.hsvAxis.set_ylim(-0.1,1.1)
        self.hsvAxis.set_axis_off()
        self.hsvAxis.invert_xaxis()
        self.hsvAxis.invert_yaxis()
        self.hsvCursor=self.hsvAxis.plot(0.5,0.5,'o',fillstyle='none')[0]
        
        self.textAxis = self.fig.add_axes([0.46,0.55,0.2,0.4]);
        self.textAxis.set_axis_off()
        textstr = '\n'.join((
            r'$\mu=%.2f$' % (1.0, ),
            r'$\mathrm{median}=%.2f$' % (1.1, ),
            r'$\sigma=%.2f$' % (0.1, )))
        self.htxt=self.textAxis.text(0.0, 1.0, textstr, transform=self.textAxis.transAxes, fontfamily='monospace', fontsize=14,
                           verticalalignment='top')

        
        angle = self.hue*np.pi/180.
        x = np.sin(-angle)*np.array([0.8,0.9]);
        y = np.cos(angle)*np.array([0.8,0.9]);
        self.hueIndicator=self.wheelAxis.plot(x,y,'k-')[0]
        
        self.previewAxis = self.fig.add_axes([0.675,0.05,0.28,0.5])
        self.previewImg = self.previewAxis.imshow(np.zeros((512,512,3),np.float32))
        # self.previewAxis.set_axis_off()
        self.previewAxis.set_xticks([])
        self.previewAxis.set_yticks([])
                
        self.fig.canvas.mpl_connect('close_event', self.handle_close)
        
        self.sliders['hue'] = axSlider(self.fig, 'hue', [0.675,0.9,0.28,0.03], self.updateSlider, maxVal=359)
        self.sliders['val'] = axSlider(self.fig, 'val', [0.675,0.85,0.28,0.03], self.updateSlider)
        self.sliders['sat'] = axSlider(self.fig, 'sat', [0.675,0.8,0.28,0.03], self.updateSlider)

        self.sliders['R'] = axSlider(self.fig, 'R', [0.675,0.7,0.28,0.03], self.updateSlider)
        self.sliders['G'] = axSlider(self.fig, 'G', [0.675,0.65,0.28,0.03], self.updateSlider)
        self.sliders['B'] = axSlider(self.fig, 'B', [0.675,0.6,0.28,0.03], self.updateSlider)
        
        cid = self.fig.canvas.mpl_connect('button_press_event', self.onclick)
        cid = self.fig.canvas.mpl_connect('button_release_event', self.onrelease)
        cid = self.fig.canvas.mpl_connect('motion_notify_event', self.onMouseMove)
        
        try:
            mpl.rcParams['keymap.back'].remove('left')
            mpl.rcParams['keymap.forward'].remove('right')
        except:
            pass;
        self.lastSlider = self.sliders['hue'];
        cid = self.fig.canvas.mpl_connect('key_press_event', self.on_keypress)

        self.updateImages();
        
        plt.show() # Replace with plt.savefig if you want to save a file
        
    def handle_close(self, evt):
        debout("close event detected ...")

    def close_event(self):
        debout("deleting colorSelection object ...")
        try:
            plt.close(self.fig)
        except:
            pass;

if __name__=="__main__":
    cfig = colorSelection()
    setattr(cfig.fig,'colorSelection',cfig);
    