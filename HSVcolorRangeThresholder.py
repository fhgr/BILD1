# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 14:09:27 2021

Open image file or Webcam. Use can select HSV range via sliders.
Original image together with a mask is displayed.
The mask is created based on the selected HSV values.
User can print the corresponding python code by pressing 'p'-key.

https://github.com/rajeevravindran/color-thresholding

@author: birkudo
"""

import cv2
import sys
import numpy as np

DEVICE_ID = "0"

max_value = 255
max_value_H = 360//2
low_H = 0
low_S = 0
low_V = 0
high_H = max_value_H
high_S = max_value
high_V = max_value
WND_NAME_IMG = 'Input image'
WND_NAME_MASK = 'Object Mask'
WND_NAME_TRACKBAR = 'HSV limits'
low_H_name = 'Low H'
low_S_name = 'Low S'
low_V_name = 'Low V'
high_H_name = 'High H'
high_S_name = 'High S'
high_V_name = 'High V'
def on_low_H_thresh_trackbar(val):
    global low_H
    global high_H
    low_H = val
    # low_H = min(high_H-1, low_H)
    cv2.setTrackbarPos(low_H_name, WND_NAME_MASK, low_H)
def on_high_H_thresh_trackbar(val):
    global low_H
    global high_H
    high_H = val
    # high_H = max(high_H, low_H+1)
    cv2.setTrackbarPos(high_H_name, WND_NAME_MASK, high_H)
def on_low_S_thresh_trackbar(val):
    global low_S
    global high_S
    low_S = val
    low_S = min(high_S-1, low_S)
    cv2.setTrackbarPos(low_S_name, WND_NAME_MASK, low_S)
def on_high_S_thresh_trackbar(val):
    global low_S
    global high_S
    high_S = val
    high_S = max(high_S, low_S+1)
    cv2.setTrackbarPos(high_S_name, WND_NAME_MASK, high_S)
def on_low_V_thresh_trackbar(val):
    global low_V
    global high_V
    low_V = val
    low_V = min(high_V-1, low_V)
    cv2.setTrackbarPos(low_V_name, WND_NAME_MASK, low_V)
def on_high_V_thresh_trackbar(val):
    global low_V
    global high_V
    high_V = val
    high_V = max(high_V, low_V+1)
    cv2.setTrackbarPos(high_V_name, WND_NAME_MASK, high_V)
   
def main():
    global DEVICE_ID
    if len(sys.argv) > 1:
        DEVICE_ID = sys.argv[1]

    isRunning = True
    if len(""+DEVICE_ID) > 1:
        bUseCamera = False;
        frame = cv2.imread(DEVICE_ID)
    else:
        bUseCamera = True;
        cap = cv2.VideoCapture(int(DEVICE_ID))
        if not cap.isOpened():
            print("ERROR: Could not open video capture.")
            isRunning = False
        else:
            print("video capture successfully opened.")
            
        
    cv2.namedWindow(WND_NAME_IMG, cv2.WINDOW_NORMAL)
    cv2.namedWindow(WND_NAME_MASK, cv2.WINDOW_NORMAL)
    cv2.namedWindow(WND_NAME_TRACKBAR, cv2.WINDOW_NORMAL)
    dummyImage = np.zeros((1,768,3), np.uint8)+255;
    cv2.imshow(WND_NAME_TRACKBAR, dummyImage)
    cv2.resizeWindow(WND_NAME_TRACKBAR, 768,1)
    cv2.createTrackbar(low_H_name, WND_NAME_TRACKBAR, low_H, max_value_H, on_low_H_thresh_trackbar)
    cv2.createTrackbar(high_H_name, WND_NAME_TRACKBAR, high_H, max_value_H, on_high_H_thresh_trackbar)
    cv2.createTrackbar(low_S_name, WND_NAME_TRACKBAR, low_S, max_value, on_low_S_thresh_trackbar)
    cv2.createTrackbar(high_S_name, WND_NAME_TRACKBAR, high_S, max_value, on_high_S_thresh_trackbar)
    cv2.createTrackbar(low_V_name, WND_NAME_TRACKBAR, low_V, max_value, on_low_V_thresh_trackbar)
    cv2.createTrackbar(high_V_name, WND_NAME_TRACKBAR, high_V, max_value, on_high_V_thresh_trackbar)
    cv2.imshow(WND_NAME_TRACKBAR, dummyImage)
    cv2.waitKey(30);
    cv2.resizeWindow(WND_NAME_TRACKBAR, 768,1)
    while isRunning:
        
        if bUseCamera:
            ret, frame = cap.read()
        if frame is None:
            print("ERROR: Could not read image.")
            break
        frame_HSV = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        
        if (low_H > high_H):
            # use two masks for both ends of the red spectrum
            lowerHSV1 = (low_H, low_S, low_V)
            upperHSV1 = (max_value_H, high_S, high_V)
            mask1 = cv2.inRange(frame_HSV, lowerHSV1, upperHSV1)
            lowerHSV2 = (0, low_S, low_V)
            upperHSV2 = (high_H, high_S, high_V)
            mask2 = cv2.inRange(frame_HSV, lowerHSV2, upperHSV2)
            frame_mask = np.bitwise_or(mask1, mask2)
        else:
            lowerHSV = (low_H, low_S, low_V)
            upperHSV = (high_H, high_S, high_V)
            frame_mask = cv2.inRange(frame_HSV, lowerHSV, upperHSV)
        
        
        cv2.imshow(WND_NAME_IMG, frame)
        cv2.imshow(WND_NAME_MASK, frame_mask)
        
        key = cv2.waitKey(30)
        if key == ord('q') or key == 27:
            break
        if key == ord('p'):
            print("");             print("");
            print('source code used to select this HSV range:')
            print("#%% ======== COPY BELOW ===========")
            print("# convert frame to HSV color space")
            print("frameHSV = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)")
            print("")
            if (low_H > high_H):
                # use two masks for both ends of the red spectrum
                print(f"lowerHSV1 = ({low_H}, {low_S}, {low_V})");
                print(f"upperHSV1 = ({max_value_H}, {low_S}, {low_V})");
                print("mask1 = cv2.inRange(frame_HSV, lowerHSV1, upperHSV1)");
                print("")
                print(f"lowerHSV2 = ({0}, {low_S}, {low_V})");
                print(f"upperHSV2 = ({high_H}, {low_S}, {low_V})");
                print("mask2 = cv2.inRange(frame_HSV, lowerHSV2, upperHSV2)");
                print("")
                print("frame_mask = np.bitwise_or(mask1, mask2)")
            else:
                print(f"lowerHSV = ({low_H}, {low_S}, {low_V})");
                print(f"upperHSV = ({high_H}, {low_S}, {low_V})");
                print("frame_mask = cv2.inRange(frame_HSV, lowerHSV, upperHSV)");
            
    
    cv2.destroyAllWindows()
    if bUseCamera:
        cap.release()
    
    
if __name__=="__main__":
    main()
    